package com.okta.spring.SpringBootOAuthClient;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.endpoint.DefaultAuthorizationCodeTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequestEntityConverter;
import org.springframework.security.oauth2.client.http.OAuth2ErrorResponseErrorHandler;
import org.springframework.security.oauth2.core.http.converter.OAuth2AccessTokenResponseHttpMessageConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Arrays;

@Slf4j
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.antMatcher("/**").authorizeRequests()
            .antMatchers("/", "/login**").permitAll()
            .anyRequest().authenticated()
            .and()
            .oauth2Login()//.and().authenticationProvider()
                .tokenEndpoint()
                    .accessTokenResponseClient(composeGetTokenEndpoint())
                .and()
//--            .and()
//--                .authenticationProvider(new OAuth2LoginAuthenticationProvider())
//                .userInfoEndpoint()
//                    .userAuthoritiesMapper(new SimpleAuthorityMapper())
//                    .userService(new DefaultOAuth2UserService())
//                    //.oidcUserService(new DefaultOAuth2UserService()).customUserType()

        ;

//        GrantedAuthoritiesMapper mapper = new SimpleAuthorityMapper();
//        OAuth2UserService
    }


    /**
     * change RestTemplate tokenEndpoint from POST to GET
     * @return
     */
    private OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> composeGetTokenEndpoint() {
        OAuth2AuthorizationCodeGrantRequestEntityConverter requestEntityConverter = new OAuth2AuthorizationCodeGrantRequestEntityConverter() {
            @Override
            public RequestEntity<?> convert(OAuth2AuthorizationCodeGrantRequest authorizationCodeGrantRequest) {
                RequestEntity<?> postRequestEntity = super.convert(authorizationCodeGrantRequest);
                assert postRequestEntity != null;
                URI uriWithQueryParametersExtractedFromBody = UriComponentsBuilder.fromUri(postRequestEntity.getUrl())
                        .queryParams((MultiValueMap<String, String>)postRequestEntity.getBody())
                        .build()
                        .toUri();
                RequestEntity<?> getRequestEntity = new RequestEntity<>(null, postRequestEntity.getHeaders(), HttpMethod.GET, uriWithQueryParametersExtractedFromBody);
                log.debug("tokenEndpoint: {}", getRequestEntity);
                return getRequestEntity;
            }
        };
        DefaultAuthorizationCodeTokenResponseClient tokenResponseClient = new DefaultAuthorizationCodeTokenResponseClient();
        tokenResponseClient.setRequestEntityConverter(requestEntityConverter);

        //todo remove this shit just for debug
        tokenResponseClient.setRestOperations(apacheLoggingRestTemplate());

        return tokenResponseClient;
    }



    /**
     * https://www.baeldung.com/spring-resttemplate-logging
     * @return
     */
    private RestTemplate apacheLoggingRestTemplate() {
        RestTemplate restTemplate = new RestTemplate(Arrays.asList(
                new FormHttpMessageConverter(), new OAuth2AccessTokenResponseHttpMessageConverter()));
        restTemplate.setErrorHandler(new OAuth2ErrorResponseErrorHandler());
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        return restTemplate;
    }
}
